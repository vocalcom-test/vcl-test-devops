# vcl-test-devops

This test is challenging candidate ability to develop a microservice to interact with AWS resources.

# Requirements

Workspace : Use a bash terminal

Technologies : 
- NPM 6.4.x
- NODE v10.6.0
- serverless 1.38.0
- cloudx-utils#0.1.5


# Questions
- Can you list the standard input formats the lib should be able to process?
- Then, technically, how do you processes inputs?
- What is a lambda?
- What is a lambda alias?
- What is a lambda configuration?

- Implements a lambda (runtime Node.js 10.x) that is updating another lambda configuration. The expecting results should describe how to test the service.

# How to test your service locally
`serverless offline start` then post data to your service

# How to deploy
- upload your code to s3
- update lambda function code from code into s3

# How to test your service in the cloud
- deploy your code to the lambda
- invoke your lambda (example : 
` aws lambda invoke --function-name arn:aws:lambda:eu-north-1:277286751135:function:cli-test --invocation-type RequestResponse somefile.txt`

Aide à la création des lambdas : 
- runtime : nodejs10.x
- role : arn:aws:iam::277286751135:role/service-role/lambdastaterole
- handler : handler.test
- S3BUcket : candidatebucket-vcl
- S3Key : you decide
- mise en forme du paramètre --code --code S3Bucket=candidatebucket-vcl,S3Key=you-decide.zip

