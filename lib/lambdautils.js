/**
 * Lambda entry point
 * @param  {any} body
 * @return  {Object} context
 */
export function success(body) {
  return buildResponse(200, body);
}

/**
  * Lambda entry point
  * @param  {boolean} isServer
  * @param  {any} body
  * @return {Object} context
  */
export function failure(isServer, body) {
  return buildResponse(isServer ? 500 : 400, body);
}

/**
  * @param  {number} statusCode
  * @param  {any} body
  * @return {Object} response object
  */
export function buildResponse(statusCode, body) {
  return {
    statusCode: statusCode,
    body: JSON.stringify(body),
  };
}
